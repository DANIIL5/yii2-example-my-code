<?php
/**
 * @var $this yii\web\View
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \common\models\User[] $models
 */

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Контрагенты';

$currentUserId = Yii::$app->user->identity->id;
$colors = [
    '#456385', '#9471db', '#6ad19f', '#4d71db', '#8cbb2d', '#db94db', '#ff7194', '#4d94db', '#db4d4d', '#71b84d',
    '#ff944d', '#4d71db', '#ff944d', '#4db8db', 'Coral', 'Crimson', 'Tomato', 'OliveDrab', 'brown', 'cadetblue',
    'darkcyan', 'steelblue', 'seagreen', 'rebeccapurple',
];
?>


<div class="page page-employee-list">

    <h1 class="page__heading"><?= $this->title ?></h1>


    <div class="page-employee-list__top-container">

        <?php /* Поиск */ ?>
        <form class="page-employee-list__filter" action="<?= Url::current() ?>" method="POST">
            <?= Html::hiddenInput(Yii::$app->request->csrfParam, Yii::$app->request->csrfToken) ?>
            <?= Html::activeTextInput($form, 'fio', [
                'placeholder' => 'Поиск по имени контрагента',
            ]); ?>
        </form>

    </div>


    <?php /* Список сотрудников */ ?>
    <div class="page-employee-list__body">
        <?= Html::beginTag('div', [
            'class' => 'employee-list js--mass',
            'data' => [
                'mass' => [
                    'id' => '_employee-list',
                ],
            ],
        ]) ?>

        <?php /* Хэдер списка с переключателями сортировки */ ?>
        <header class="employee-list__header">

            <div class="employee-list__header-inner-wrapper">

                <?php /* Имя и должность */ ?>
                <div class="employee-list__header-cell employee-list__header-cell_name-and-post">
                    <?php $turn = $dataProvider->sort->getAttributeOrder('name'); ?>
                    <?= Html::a('Контрагент', $dataProvider->sort->createUrl('name'), [
                        'class' => [
                            'sort-toggler',
                            $turn ? 'active' : null,
                            $turn === SORT_DESC ? 'reverse' : null,
                        ],
                    ]) ?>
                </div>

                <?php /* Менеджер по ведению */ ?>
                <div class="employee-list__header-cell employee-list__header-cell_email">
                    <?php $turn = $dataProvider->sort->getAttributeOrder('manager_client'); ?>
                    <?= Html::a('Менеджер по ведению', $dataProvider->sort->createUrl('manager_client'), [
                        'class' => [
                            'sort-toggler',
                            $turn ? 'active' : null,
                            $turn === SORT_DESC ? 'reverse' : null,
                        ],
                    ]) ?>
                </div>


                <?php /* Менеджер по развитию */ ?>
                <div class="employee-list__header-cell employee-list__header-cell_email">
                    <?php $turn = $dataProvider->sort->getAttributeOrder('manager_development'); ?>
                    <?= Html::a('Менеджер по развитию', $dataProvider->sort->createUrl('manager_development'), [
                        'class' => [
                            'sort-toggler',
                            $turn ? 'active' : null,
                            $turn === SORT_DESC ? 'reverse' : null,
                        ],
                    ]) ?>
                </div>

            </div>

        </header>


        <?php /* Список контрагентов менеджера */ ?>
        <div class="employee-list__body">

            <?php foreach ($dataProvider->models as $key => $model): ?>
                <div class="employee-list__row mass-item" id="employee-list-<?= $key ?>">

                    <div class="employee-list__row-wrapper employee-list__row-wrapper_left">

                        <?php /* Кнопка-раскрывашка информации пользователя в мобильной версии */ ?>
                        <?= Html::button('', [
                            'class' => 'employee-list__toggler js--toggle-class',
                            'data' => [
                                'toggle-class' => [
                                    'target' => '.employee-list__row <<',
                                ],
                            ],
                        ]) ?>


                        <?php /* Аватар */ ?>
                        <div class="employee-list__cell employee-list__cell_image">
                            <a class="employee-list__image-link" href="<?= Url::to(['/user/profile', 'id' => $model['id']]) ?>">
                                    <span class="employee-list__image employee-list__image_empty" style="background-color: <?= $colors[crc32($model['id']) % count($colors)] ?>;">
                                            <?= mb_substr($model['name'], 0, 1, 'utf8') . mb_substr($model['name'], 0, 1, 'utf8') ?>
                                        </span>
                            </a>
                        </div>


                        <?php /* Имя и должность */ ?>
                        <div class="employee-list__cell employee-list__cell_name-and-post">
                            <a class="employee-list__name" href="<?= Url::to(['/partner/profile', 'id' =>$model['id']]) ?>"><?= $model['name'] ?></a>
                            <span class="employee-list__post"><?= $model['status'] ?></span>
                        </div>

                    </div>


                    <?php /* Блок с этой информацией переключается в мобильной версии */ ?>
                    <div class="employee-list__row-wrapper employee-list__row-wrapper_right">

                        <?php /* Менеджер по ведению */ ?>
                        <div class="employee-list__cell employee-list__cell_access-level">
                            <span class="employee-list__cell-title">Менеджер по ведению</span>
                            <span class="employee-list__access-level employee-list__cell-text"><?= $model['manager_development'] ?></span>
                        </div>


                        <?php /* Менеджер по развитию */ ?>
                        <div class="employee-list__cell employee-list__cell_access-level">
                            <span class="employee-list__cell-title">Менеджер по ведению</span>
                            <span class="employee-list__access-level employee-list__cell-text"><?= $model['manager_client'] ?></span>
                        </div>


                        <?php /* Кнопка перехода в профиль сотрудника (мобильная версия) */ ?>
                        <a class="btn2 employee-list__profile-link btn2_color_default btn2_full-width" href="<?= Url::to(['/user/update', 'id' => $model['id']]) ?>">
                                <span class="btn2__wrapper">
                                    <span class="btn2__text">Редактировать</span>
                                </span>
                        </a>

                    </div>

                </div>
            <?php endforeach; ?>

        </div>

        <?= Html::endTag('div') ?>
    </div>


    <?php /* Пагинация */ ?>
    <div class="page-employee-list__pagination">
        <?= \yii\widgets\LinkPager::widget(
            [
                'options' => [
                    'tag' => 'div',
                    'class' => 'pagination',
                ],
                'activePageCssClass' => 'pagination__item_active',
                'linkContainerOptions' => [
                    'tag' => 'div',
                    'class' => 'pagination__item',
                ],
                'linkOptions' => [
                    'class' => 'pagination__btn',
                ],
                'disabledPageCssClass' => 'pagination__item_disabled',
                'disabledListItemSubTagOptions' => [
                    'tag' => 'button',
                    'class' => 'pagination__btn',
                    'type' => 'button',
                    'disabled' => true,
                ],
                'maxButtonCount' => 5,
                'nextPageCssClass' => 'pagination__next',
                'prevPageCssClass' => 'pagination__prev',
                'pagination' => $dataProvider->pagination,
            ]
        ) ?>
    </div>

</div>
