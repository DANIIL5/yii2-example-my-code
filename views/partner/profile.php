<?php
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\User;
use common\widgets\formField\FormField;

/**
 * @var \yii\web\View $this
 * @var \common\models\User $user
 * @var array $notifications
 * @var array  $partner
 */

$this->title = 'Настройки';
?>
<div class="page page-profile">
    <h1 class="page__heading"><?= $this->title ?></h1>
    <div class="page-profile__mainbody">
        <div class="page__panel page__panel_with-border">
            <div class="tabs2 tabs2_theme_modern js--tabs2">
                <?php /* вкладоки десктоп + контент вкладок */ ?>
                <div class="page-profile__content-wrapper">
                    <?php /* десктоп вкладки */ ?>
                    <?php /* контент вкладок */ ?>
                    <div class="tabs2__content">
                        <?php /* Вкладка «Основные» */ ?>
                        <button class="btn2 btn2_color_default btn2_full-width-mobile"
                                name="action"
                                value="submit,noBrandingOneWarehouse"
                                form="cart_cart"
                        >
                        <span class="btn2__wrapper">
                            <span class="btn2__text">  <a class="menu__link" <?= Html::a('Назад', Yii::$app->request->referrer)?>
                                </a></span>
                        </span>
                        </button>
                        <div class="tabs2__tab tabs2__tab_active" id="general">
                            <form class="page-profile__general" action="<?= Url::current() ?>" method="post" data-via="ajax">
                                <?= Html::hiddenInput(Yii::$app->request->csrfParam, Yii::$app->request->csrfToken) ?>
                                <div class="page-profile__general-body">
                                    <?php $field = FormField::field($partner, 'birthday'); ?>
                                        <div class="textfield textfield_addon-before-nobg">
                                            <div class="textfield__wrapper">
                                                <div class="textfield__addon textfield__addon_before">
                                                    <span class="textfield__addon-icon oasisicon-calendar"></span>
                                                </div>
                                                <?= Html::activeTextInput($partner, 'birthday', [
                                                    'noStyle' => true,
                                                    'class'   => 'textfield__control js--datepicker',
                                                    'id'      => $field->id,
                                                    'value'   => $partner->birthday ? Yii::$app->formatter->asDate($partner->birthday, 'php:Y-m-d') : null,
                                                ]) ?>
                                            </div>
                                        </div>
                                    <?= $field->run() ?>
                                </div>
                                <footer class="page-profile__general-footer">
                                    <button class="btn2 btn2_color_success btn2_full-width-mobile">
                                        <span class="btn2__wrapper">
                                            <span class="btn2__text">Сохранить</span>
                                        </span>
                                    </button>
                                </footer>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
