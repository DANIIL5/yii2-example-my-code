<?php

namespace oasis\controllers;

use common\components\ArrayDataProvider;
use common\models\App;
use common\models\Partner;
use common\models\PartnerCategory;
use common\models\StatVisit;
use oasis\models\forms\UserForm;
use Yii;
use yii\data\SqlDataProvider;
use yii\db\Expression;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;
use common\components\Controller;
use common\jobs\UserCreateJob;
use common\widgets\Alert;
use common\models\User;
use oasis\actions\NotificationSettingsAction;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;

class PartnerController extends Controller
{

    public $layout = 'cabinet';


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['manager'],
                        'actions' => ['profile'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['manager'],
                    ],
                ],
            ],
            [
                'class' => 'yii\filters\VerbFilter',
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Список Контрагентов менеджера
     *
     * @return string
     */
    public function actionCounterParties()
    {
        $form = new UserForm();
        $form->load(Yii::$app->request->post());
        $partners = Partner::find(false, 'p')
            ->allowed(null, true)
            ->select(
                [
                    'id' => 'p.id',
                    'name' => 'p.name',
                    'manager_development' => 'manager_development.name',
                    'manager_client' => 'manager_client.name',
                    'status' => 'category.name',
                ]
            )
            ->leftJoin(['manager_development' => 'manager'], "p.manager_development = manager_development.id")
            ->leftJoin(['manager_client' => 'manager'], "p.manager_client = manager_client.id")
            ->leftJoin(['category' => 'partner_category'], "category.id = p.category_id");
        if ($form->fio) {
            $partners->andFilterWhere(
                [
                    'or',
                    ['ilike', 'p.name', $form->fio],
                ]
            );
        }

        $models = new ArrayDataProvider(
            [
                'allModels' => $partners->asArray()->all(),
                'pagination' => [
                    'pageSize' => 100,
                ],
                'sort' => [
                    'attributes' => ['id', 'name', 'manager_development', 'manager_client', 'status'],
                ],
            ]
        );

        return $this->render(
            'index',
            [
                'dataProvider' => $models,
                'form' => $form,
            ]
        );
    }

    /**
     * Редактировать профиль
     *
     * @param null $id
     *
     * @return array|string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionProfile($id = null)
    {
        $partner = Partner::find(false, 'p')
            ->select(
                [
                    '*',

                ]
            )
            ->where('id = :id', [':id' => $id])->one();
        if ($partner->load(Yii::$app->request->post())) {
            if ($partner->save()) {
                return $this->flash('Изменения сохранены', Alert::TYPE_SUCCESS);
            } elseif (Yii::$app->request->isAjax) {
                return $this->flash(Html::errorSummary($partner, ['header' => '']), Alert::TYPE_DANGER);
            }
        }

        return $this->render(
            'profile',
            [
                'partner' => $partner,
            ]
        );
    }

}
