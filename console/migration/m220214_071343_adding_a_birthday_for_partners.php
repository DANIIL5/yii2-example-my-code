<?php

namespace console\migrations;

use yii\db\Migration;
use yii\db\pgsql\QueryBuilder;

class m220214_071343_adding_a_birthday_for_partners extends Migration
{

    public function safeUp()
    {
        $this->addColumn('{{%partner}}', 'birthday', $this->date());
        $this->addCommentOnColumn('{{%partner}}', 'birthday', 'День рождения партнера');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%partner}}', 'birthday');
    }

}
