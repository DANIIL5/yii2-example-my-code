<?php

namespace console\migrations;

use Yii;
use yii\db\Migration;

/**
 * Class m220210082348_adding_the_counterparties_link_to_the_menu
 */
class m220210_082348_adding_the_counterparties_link_to_the_menu extends Migration
{

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert(
            'cmyii_page',
            [
                'site_id' => '10',
                'parent_id' => '41',
                'sort' => 1110,
                'depth' => 3,
                'slug' => 'counterparties',
                'title' => 'Контрагенты',
                'path' => '/_menu/cabinet/counter-parties',
                'link' => '/partner/counter-parties',
                'is_disabled' => false,
                'roles' => 'manager',
                'css_classes' => 'oasisicon-workers',
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('cmyii_page', ['slug' => 'counterparties']);
        return true;
    }

}
