<?php

namespace common\models;

use common\components\mailer\Message;
use common\helpers\ActiveRecordHelper;
use common\jobs\BonusSetJob;
use common\jobs\planfix\PartnerJob;
use paulzi\jsonBehavior\JsonBehavior;
use paulzi\jsonBehavior\JsonField;
use paulzi\jsonBehavior\JsonValidator;
use phpDocumentor\Reflection\Types\Boolean;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;

/**
 * This is the model class for table "{{%partner}}".
 *
 * @property string $id
 * @property string $name
 * @property string $region_id
 * @property string $category_id
 * @property integer $status_id
 * @property string $contract_id
 * @property string $org_id
 * @property string $agency_id
 * @property integer $manager_client
 * @property integer $manager_development
 * @property string $api_token
 * @property integer $credit_limit
 * @property integer $samples_credit_limit
 * @property integer $credit_time
 * @property boolean $is_dealer
 * @property boolean $premium_brand_allowed
 * @property boolean $no_vat
 * @property string $created_at
 * @property string $updated_at
 * @property boolean $is_deleted
 * @property JsonField $params
 *
 * @property Contract $contractDefault
 * @property Contract[] $contracts
 * @property Company[] $companies
 * @property Region $region
 * @property PartnerCategory $category
 * @property Organization $defaultOrganization
 * @property Partner $agency
 * @property Manager $managerClient
 * @property Manager $managerDevelopment
 * @property Dealer $dealer
 * @property PartnerDiscount[] $discounts
 * @property PartnerSumPlan[] $planPoints
 * @property User[] $users
 * @property User $firstUser
 * @property Collection[] $publicCollections
 * @property Collection[] $oasisCollections
 * @property Collection[] $collections
 * @property User[] $chiefs
 * @property bool $isHasMarketingBonus
 * @property Order[] $orders
 * @property PartnerAddress[] $addresses
 *
 * @property int $genericCreditUsed
 * @property int $sampleCreditUsed
 * @property bool $isOasis
 * @property float $currentRedemption
 * @property float $freeDeliveryThresholdFactor
 * @property int|null $freeDeliverySum
 * @property PartnerSamplePreference $samplePreference
 * @property SampleBonus[] $sampleBonuses
 * @property SampleDistribution[] $sampleDistributions
 * @property bool $allowCredit
 * @property bool $isFreeDeliveryBonus
 * @property bool $isInPromotionLeague
 * @property bool $hasOwnProduction
 * @property bool $needToExcludeBranding
 * @property bool $forbiddenSeasonBranding
 * @property string $birthday
 */
class Partner extends \yii\db\ActiveRecord
{
    const STATUS_BUSINESS_PARTNER    = 1; // Бизнес-партнер
    const STATUS_DEALER              = 2; // Дилер
    const STATUS_DISTRIBUTOR         = 3; // Дистрибутор
    const STATUS_BUYER               = 4; // Покупатель
    const STATUS_PARTNER             = 5; // Партнер
    const STATUS_FEDERAL_DEALER      = 6; // Федеральный Дилер
    const STATUS_FEDERAL_DISTRIBUTOR = 7; // Федеральный Дистрибьютор
    const STATUS_ONLINE_SHOP         = 8; // Интернет магазин
    const STATUS_END_USER            = 9; // Конечный заказчик

    const PARTNER_OASIS = '000004557';

    private $_bonusCoupons = [];

    public $total;
    public $delay;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%partner}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class'      => JsonBehavior::class,
                'attributes' => ['params'],
                'emptyValue' => '{}',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['status_id', 'manager_client', 'manager_development', 'credit_limit', 'samples_credit_limit', 'credit_time'], 'integer'],
            [['is_dealer', 'premium_brand_allowed', 'no_vat', 'is_deleted'], 'boolean'],
            [['created_at', 'updated_at'], 'safe'],
            [['id', 'region_id', 'category_id', 'contract_id', 'org_id', 'agency_id'], 'string', 'max' => 9],
            [['name'], 'string', 'max' => 100],
            [['api_token'], 'string', 'max' => 40],
            [['params'], JsonValidator::class, 'merge' => true, 'on' => 'admin'],
            [['birthday'], 'date',
                'format'                   => 'php:d.m.Y',
                'timestampAttribute'       => 'birthday',
                'timestampAttributeFormat' => 'php:Y-m-d',
            ],
            [['birthday'], 'default', 'value' => null],
            [['params'], 'filter', 'filter' => function($value) {
                /** @var JsonField $value */
                $value['bonuses'] = !empty($value['bonuses']) ? array_values($value['bonuses']) : null;
                if (($value['fBonusesAmount'] ?? null) === '') {
                    unset($value['fBonusesAmount']);
                }
                $fBonuses = [];
                foreach ($value['fBonuses'] ?? [] as $k => $item) {
                    if ($item['allowed']) {
                        $fBonuses[$k] = $item;
                    }
                }
                if ($fBonuses) {
                    $value['fBonuses'] = $fBonuses;
                } else {
                    unset($value['fBonuses']);
                }
                return array_filter($value->toArray(), function($v) { return $v !== null; });
            }, 'on' => 'admin'],
        ];
    }

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidConfigException
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if (isset($changedAttributes['params'])) {
            $old = new JsonField($changedAttributes['params']);
            if (Json::encode($old['bonuses'] ?? null) !== Json::encode($this->params['bonuses'] ?? null)) {
                $set = [];
                foreach ($this->params['bonuses'] ?? [] as $bonus) {
                    if ($bonus['type'] === 'brand') {
                        $bonus['id'] = substr($bonus['id'], -5);
                    }
                    if (isset($bonus['value']) && is_numeric($bonus['value'])) {
                        $bonus['value'] = (float)$bonus['value'];
                    }
                    $set[] = $bonus;
                }

                $job = new BonusSetJob();
                $job->partnerId = $this->id;
                $job->set = $set;
                Yii::$app->queue->push($job);
            }
        }
    }

    /**
     * Основной договор
     * @return \yii\db\ActiveQuery
     */
    public function getContractDefault()
    {
        return $this->hasOne(Contract::className(), ['id' => 'contract_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContracts()
    {
        return $this->hasMany(Contract::className(), ['partner_id' => 'id'])
            ->with('company')
            ->orderBy(['is_credit' => SORT_DESC, 'name' => SORT_ASC])
            ->indexBy('id')
            ->inverseOf('partner');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['id' => 'company_id'])
            ->viaTable('contract', ['partner_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(PartnerCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDefaultOrganization()
    {
        return $this->hasOne(Organization::className(), ['id' => 'org_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgency()
    {
        return $this->hasOne(static::className(), ['id' => 'agency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManagerClient()
    {
        return $this->hasOne(Manager::className(), ['id' => 'manager_client']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManagerDevelopment()
    {
        return $this->hasOne(Manager::className(), ['id' => 'manager_development']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDealer()
    {
        return $this->hasOne(Dealer::className(), ['partner_id' => 'id'])
            ->inverseOf('partner');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscounts()
    {
        return $this->hasMany(PartnerDiscount::className(), ['partner_id' => 'id'])
            ->indexBy('discount_id')
            ->inverseOf('partner');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlanPoints()
    {
        return $this->hasMany(PartnerSumPlan::className(), ['partner_id' => 'id'])
            ->indexBy('date')
            ->inverseOf('partner');
    }

    /**
     * Пользователи партнёра
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['partner_id' => 'id'])
            ->orderBy(['first_name' => SORT_ASC, 'last_name' => SORT_ASC])
            ->indexBy('id');
    }

    /**
     * Первый пользователь партнёра
     * @return \yii\db\ActiveQuery
     */
    public function getFirstUser()
    {
        return $this->hasOne(User::className(), ['partner_id' => 'id'])
            ->orderBy(['first_name' => SORT_ASC, 'last_name' => SORT_ASC])
            ->indexBy('id');
    }

    /**
     * Руководители партнёра (только для связок), для получения руководителей используй getChief()
     * @return \yii\db\ActiveQuery
     */
    public function getChiefUsers()
    {
        return $this->getUsers()
            ->andWhere(['role_id' => UserRole::ROLE_CHIEF]);
    }

    /**
     * Заказы партнёра
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        $query = Order::find(true);
        $query->primaryModel = $this;
        $query->link = ['partner_id' => 'id'];
        $query->multiple = true;
        return $query;
    }

    /**
     * Адреса партнёров
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(PartnerAddress::class, ['partner_id' => 'id'])
            ->orderBy(['last_usage' => SORT_DESC])
            ->indexBy('id');
    }

    /**
     * Сортировка пользователей
     * @param array $field
     * @return User[]
     */
    public function getUsersOrderBy($field = ['last_name', 'first_name'])
    {
        $result = $this->users;
        ArrayHelper::multisort($result, $field);
        return $result;
    }

    /**
     * @inheritdoc
     * @param bool $all
     * @param string|null $alias
     * @return PartnerQuery
     */
    public static function find($all = false, $alias = null)
    {
        $query = new PartnerQuery(get_called_class());
        if ($alias) {
            $query->alias($alias);
        }
        return $all ? $query : $query->active();
    }

    /**
     * Публичные коллекции партнёра
     * @return \yii\db\ActiveQuery
     */
    public function getPublicCollections()
    {
        $query = Collection::find()
            ->andWhere([
                'user_id' => array_keys($this->users),
                'access'  => [Collection::ACCESS_PUBLIC_VIEW, Collection::ACCESS_PUBLIC_EDIT],
            ])
            ->indexBy('id');
        $query->multiple = true;
        return $query;
    }

    /**
     * Возвращает все коллекции партнёра
     * @return \yii\db\ActiveQuery
     */
    public function getCollections()
    {
        $query = Collection::find()
            ->andWhere([
                'user_id' => array_keys($this->users),
            ])
            ->indexBy('id');
        $query->multiple = true;
        return $query;
    }

    /**
     * Возвращает все коллекции, созданные менеджерами Oasis'a, доступные партнёру
     * @return \yii\db\ActiveQuery
     */
    public function getOasisCollections()
    {
        $query = Collection::find()
            ->alias('c')
            ->andWhere('c.params @> \'{"partners":["' . $this->id . '"]}\'::jsonb')
            ->andWhere([
                'c.access' => [Collection::ACCESS_PARTNER_VIEW, Collection::ACCESS_PARTNER_EDIT],
            ])
            ->indexBy('id');
        $query->multiple = true;

        return $query;
    }

    /**
     * Возвращает ID коллекций, в которых есть группа товаров
     * @param string $groupId
     * @return array
     */
    public function getCollectionsIdsByGroup(string $groupId)
    {
        $data = CollectionItem::find()
            ->alias('ci')
            ->distinct()
            ->innerJoinWith('collection c')
            ->innerJoinWith('productRelation p')
            ->select('ci.[[collection_id]]')
            ->andWhere(['COALESCE(p.[[parent_size_id]], p.[[id]])' => $groupId])
            ->andWhere(['c.[[user_id]]' => array_keys($this->users)])
            ->column();
        return array_combine($data, $data);
    }

    /**
     * Возвращает сумму использованного кредита на обычные заказы
     * @return int
     */
    public function getGenericCreditUsed() : int
    {
        if ($this->isRelationPopulated('genericCreditUsed')) {
            return $this->relatedRecords['genericCreditUsed'];
        }
        $result = OrderCredit::find()
            ->andWhere([
                'partner_id' => $this->id,
                'type'       => OrderCredit::TYPE_GENERIC,
            ])
            ->sum('total');
        $result = (int)$result;
        $this->populateRelation('genericCreditUsed', $result);

        return $result;
    }

    /**
     * Возвращает сумму использованного кредита на образцы
     * @return int
     */
    public function getSampleCreditUsed() : int
    {
        if ($this->isRelationPopulated('sampleCreditUsed')) {
            return $this->relatedRecords['sampleCreditUsed'];
        }
        $subQuery = OrderProduct::find()
            ->select(['orpr.order_id', 'orpr.product_id', 'orpr.sum'])
            ->alias('orpr')
            ->joinWith(['collectionRelation o'])
            ->andWhere(['not in', 'status', [Order::STATUS_CLOSED]])
            ->andWhere(['>=', 'created_at', '2015-01-01'])
            ->andWhere([
                'orpr.is_service' => false,
                'o.org_id' => '000000006', //первая образцовая
                'o.partner_id' => $this->id,
                'o.is_deleted' => false,
            ])
            ->groupBy(['orpr.order_id', 'orpr.product_id', 'orpr.sum']);

        $result = ProductReservation::find()
            ->alias('prrsrv')
            ->select('SUM(t.sum)')
            ->innerJoin(['t' => $subQuery], 't.order_id = prrsrv.order_id and prrsrv.product_id = t.product_id')
            ->andWhere(['prrsrv.warehouse_id' => [Warehouse::WH_SAMPLE_GIVEN_MSK, Warehouse::WH_SAMPLE_GIVEN_SPB]])
            ->scalar();
        $result = (int)$result;
        $this->populateRelation('sampleCreditUsed', $result);

        return $result;
    }

    /**
     * Возвращает email для оповещения о заказах на образцы
     * @return string|array
     */
    public function getSamplesInbox()
    {
        return Yii::$app->params[$this->region->isSpb ? 'sampleEmailSpb' : 'sampleEmailMain'];
    }

    /**
     * Возвращает руководителей
     * @return array
     */
    public function getChiefs() : array
    {
        if ($this->isRelationPopulated('users')) {
            $result = [];
            foreach ($this->users as $user) {
                if ($user->role_id === UserRole::ROLE_CHIEF) {
                    $result[$user->id] = $user;
                }
            }
        } else {
            $result = $this->getChiefUsers()->all();
        }
        $this->populateRelation('chiefs', $result);
        return $result;
    }

    /**
     * Возвращает список пользователей для получения уведомлений руководителю
     * @return User[]
     */
    public function getChiefNotificationList() : array
    {
        $result = [];
        $emails = [];
        foreach ($this->users as $user) {
            if (in_array($user->role_id, [UserRole::ROLE_CHIEF, UserRole::ROLE_SENIOR_MANAGER], true)) {
                $result[$user->id] = $user;
                $emails[] = $user->email;
            }
        }
        EmailUnsubscribe::populateList($emails);
        ActiveRecordHelper::populateWith($result, ['notificationSettings']);
        return $result;
    }

    /**
     * Проверяет открыта ли кредитная линия у партнёра
     * @return bool
     */
    public static function hasCredit()
    {
        /** @var Partner $model */
        $model = self::find()
            ->select(['credit_limit'])
            ->where(['id' => Yii::$app->user->identity->partner_id])
            ->one();

        return ($model && $model->credit_limit);
    }

    /**
     * Сумма заказов по КЛ
     * @return mixed
     * @deprecated use getGenericCreditUsed
     */
    public function getCreditTaken()
    {
        return $this->getGenericCreditUsed();
    }

    /**
     * Проверка, есть ли у Партнёра договор с разрешённой КЛ
     * @return bool
     */
    public function getHasCreditContract()
    {
        foreach ($this->contracts as $contract) {
            if ($contract->contract_type == Contract::TYPE_BUYER && $contract->is_credit == 1) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarketingBonus()
    {
        return $this->hasOne(MarketingBonus::className(), ['partner_id' => 'id'])
            ->inverseOf('partner');
    }

    /**
     * Есть ли возможность у партнёра использовать МБ
     * Зависит от статуса партнёра
     * @return bool
     */
    public function getIsHasMarketingBonus()
    {
        $categories = [
            '000000065', //Сталь
            '000000059', //Хром
            '000000067', //Бронза
            '000000069', //Серебро
            '000000066', //Золото
            '000000070', //Платина
            '000000062', //Сапфир
            '000000068', //Алмаз
            '000000072', //Единорог
        ];
        return in_array($this->category_id, $categories);
    }

    /**
     * Запрос получения кол-ва новых гостевых заказов
     * @return mixed
     */
    public function getNewGuestOrdersQuery()
    {
        return GuestOrder::find()
            ->alias('g')
            ->innerJoinWith('offers o', false)
            ->andWhere([
                'o.partner_id' => Yii::$app->user->partner->id,
                'g.status'     => GuestOrder::STATUS_NEW,
                'o.status'     => GuestOrderOffer::STATUS_NEW,
            ]);
    }

    /**
     * @return bool
     */
    public function getIsOasis()
    {
        return $this->id === static::PARTNER_OASIS;
    }

    /**
     * Процент выкупаемости
     * @return float
     */
    public function getCurrentRedemption()
    {
        //@fixme Удалить. Тестирование Бачманова
        if (Yii::$app->has('user') && Yii::$app->user->id == '1-0013484') {
            return 23;
        }

        $curRedemption = Order::find()
            ->generic()
            ->select(['value' => 'SUM(COALESCE(sell_total,0)) / SUM(total)'])
            ->andWhere(['partner_id' => $this->id])
            ->andWhere('created_at::date >= :from', [':from' => date('Y-m-d', strtotime('-1 year'))])
            ->andWhere('created_at::date <= :to',   [':to'   => date('Y-m-d')])
            ->scalar();

        return round($curRedemption, 2) * 100;
    }

    /**
     * Возвращает коэффициент порога бесплатной доставки для партнёра
     * @return float
     */
    public function getFreeDeliveryThresholdFactor()
    {
        if ($bonuses = $this->params['bonuses'] ?? []) {
            foreach ($bonuses as $bonus) {
                if ($bonus['type'] === 'freeDelivery') {
                    return 1 - $bonus['value'] / 100;
                }
            }
        }
        return 1.0;
    }

    /**
     * Возвращает сумму бесплатной достаки для партнёра
     * или пользователя, если для него указан отличный от партнёра регион
     * @return int|null
     */
    public function getFreeDeliverySum()
    {
        if (Yii::$app->user->identity->region) {
            $result = Yii::$app->user->identity->region->free_delivery_sum;
        } else {
            $result = $this->region->free_delivery_sum ?? null;
        }
        if ($result) {
            $result *= $this->getFreeDeliveryThresholdFactor();
        }
        return $result;
    }

    public function getFreeDeliveryData()
    {
        $factor  = $this->freeDeliveryThresholdFactor ?? 1.0;
        $baikalQuery = Region::find()
            ->select([
                'transport_company_id' => new Expression("'00009'"),
                'service_id'           => new Expression("'00000028902'"),
                'region_id'            => 'id',
                'sum_min'              => new Expression('round(free_delivery_sum::float * :factor)'),
                'sum_max'              => new Expression('null'),
                'priority'             => new Expression('-2000'),
            ])
            ->andWhere(['>', 'free_delivery_sum', 0])
            ->andWhere(['not in', 'id', [ // Байкал не показываем
                '000000002', // в Москве
                '000000009', // в Питере
                '000000046', // Алма-Аты
                '000000053', // Астана
                '1-0000147', // Костанай
                '1-0000151', // Бишкек
            ]])
            ->addParams([':factor' => $factor]);

        $subQuery = TransportCompanyService::find()
            ->select([
                'transport_company_id',
                'service_id',
                'region_id',
                'sum_min',
                'sum_max',
                'priority',
            ])
            ->andWhere(['is_exclusion' => false])
            ->union($baikalQuery, true);

        $excludeQuery =  TransportCompanyService::find()
            ->andWhere("transport_company_id = m.transport_company_id")
            ->andWhere("service_id = m.service_id")
            ->andWhere(['region_id' => $this->region_id])
            ->andWhere(['is_exclusion' => true]);

        $query = Product::find(true, 'p')
            ->select(['p.name', 'p.id', 'm.transport_company_id', 'm.sum_min', 'sum_max'])
            ->innerJoin(['m' => $subQuery], "m.service_id = p.id")
            ->leftJoin(['r' => Region::tableName()], "r.id = m.region_id")
            ->andWhere([
                'p.is_deleted' => false,
                'p.type_id'    => Product::TYPE_DELIVERY, // вид товара - доставка
            ])
            ->andWhere(['not exists', $excludeQuery])
            ->andWhere(['m.region_id' => $this->region_id])
            ->andWhere(['not in', 'p.id', [ // исключаем хардкодом самовывозы
                '1-000025142', // Самовывоз Москва
                '1-000025143', // Самовывоз Петербург
                '1-000025144', // Доставка Новосибирск
                '1-000056833', // Самовывоз Одинцово
                '00000011831', // Транспортные услуги в пределах МКАД (600 руб.)
            ]])
            ->groupBy(['p.name', 'p.id', 'm.transport_company_id', 'm.sum_min', 'sum_max'])
            ->orderBy([
                new Expression("MIN(m.priority)"),
                'p.full_name' => SORT_ASC,
            ])
            ->indexBy('id')
            ->asArray();

        $rows = $query->all();
        if ($rows) {
            foreach ($rows as &$row) {
                if ($row['transport_company_id'] == '00009') { // затычка для Байкал-Сервиса
                    $row['sum_min'] = $this->region->free_delivery_sum;
                }
            }
        }

        return $rows;
    }

    /**
     * Возвращает факт наличия бонуса
     * @return bool
     */
    public function getIsFreeDeliveryBonus()
    {
        $result = false;
        if ($bonuses = $this->params['bonuses'] ?? []) {
            foreach ($bonuses as $bonus) {
                if ($bonus['type'] === 'freeDelivery') {
                    $result = true;
                    break;
                }
            }
        }
        return $result;
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'birthday'          => 'День рождения партнера',
        ];
    }

    /**
     * Список плюшек
     * @return array
     */
    public static function getBonusTypeList()
    {
        return [
            'discount' => [
                'label'   => 'Дополнительная скидка на выбранную категорию',
                'tooltip' => 'Выберите одну из предложенных товарных категорий Oasis Catalog и получите к вашей текущей скидке дополнительную скидку 2%',
                'modal'   => Url::to(['/discounts/bonus-discount']),
            ],
            'freeDelivery' => [
                'label'         => 'Уменьшение суммы заказа для бесплатной доставки на 30%',
                'tooltipAsHtml' => true,
                'tooltip'       => 'Бесплатная доставка по Москве и Санкт-Петербургу компанией Oasis возможна от суммы заказа в 15 000 рублей. Бесплатная доставка в регионы – от разных сумм, в зависимости от региона.<br>
                    Ознакомиться с подробной информацией можно в разделе «<a href="/delivery-price" class="link link_dashed" target="_blank">Доставка</a>». Выбрав эту привилегию, сумма заказа для бесплатной доставки будет уменьшена на 30%.',
            ],
            'returnPeriod' => [
                'label'   => 'Увеличение срока подачи рекламации',
                'tooltip' => 'Сроки для подачи рекламации увеличиваются в 2 раза. Соответственно, при выборе данной привилегии подать рекламацию на товар категории Бизнес можно будет на протяжении 20 рабочих дней, на товар категории VIP – 10 рабочих дней, а на бой – 4 рабочих дней.',
            ],
            'reservePeriod' => [
                'label'   => 'Увеличение срока резерва на 2 дня',
                'tooltip' => 'Резерв, поставленный самостоятельно на сайте, автоматически ставится на срок, превышающий стандартный на 2 рабочих дня.',
            ],
            'creditPeriod' => [
                'label'   => 'Увеличение кредитной линии до 45 дней',
                'tooltip' => 'Срок погашения задолженности по заказам, отпущенным в кредит, будет составлять 45 дней.',
            ],
            /*
            'brandingTime' => [
                'label'   => 'Сокращённые сроки нанесения',
                'tooltip' => 'При выборе этой привилегии вы получите свой заказ с нанесением в более короткие сроки. Не распространяется на такие виды нанесения, как деколь, вышивка, шубер, металлостикер, цифровая печать и сублимация.',
            ],
            */
            'cashback' => [
                'label'   => 'Начисление кэшбека',
                'tooltip' => 'При выборе этой привилегии при достижении контрольной точки вам будет начислен cash back, согласно условиям дополнительного соглашения, подробности уточняйте у своего персонального менеджера по развитию.',
            ],
            'bigMBonus' => [
                'label'   => 'Увеличение МБ',
                'tooltip' => 'При выборе этой привилегии ваш маркетинговый бонус будет увеличен до 2%',
            ],
            'freeBranding' => [
                'label'   => 'Нанесение за 1 рубль',
                'tooltip' => 'При выборе данной привилегии у вас есть возможность делать нанесение всего за рубль. Круговая трафаретная печать на бутылках, термокружках, термосах при тираже от 100 штук.',
            ],
            'freeSetup' => [
                'label' => 'Бесплатная настройка тиража',
                'tooltip' => 'При выборе данной привилегии настройка тиража будет бесплатной',
            ],
            'reserveForTender' => [
                'label' => 'Резерв под тендер',
                'tooltip' => 'При выборе данной привилегии появляется возможность держать резерв под тендер 10 рабочих дней вне зависимости от сезонности. На данные резервы также не распространяется правило обезличенного резерва. Чтобы установить данный тип резерва - сообщите его номер своему персональному менеджеру',
            ],
            'brandingDisc' => [
                'label' => 'Дополнительная скидка на нанесение',
                'tooltip' => 'Выберите один из предложенных видов нанесения и получите к вашей текущей скидке дополнительную скидку в 5%',
                'modal'   => Url::to(['/discounts/bonus-branding-disc']),
            ],
        ];
    }

    public static function getBonusTypeListExpand()
    {
        $result = [
            'category' => [
                'label' => 'Дополнительная скидка на выбранную категорию',
            ],
            'brand' => [
                'label' => 'Дополнительная скидка на выбранный бренд',
            ],
            'product' => [
                'label' => 'Дополнительная скидка на выбранный товар',
            ],
            'brandingTypeDisc' => [
                'label' => 'Дополнительная скидка на выбранный тип нанесения',
            ],
            'brandingColorDisc' => [
                'label' => 'Дополнительная скидка на выбранную услугу нанесения',
            ],
        ];
        return array_merge($result, array_diff_key(static::getBonusTypeList(), [
            'discount'     => true,
            'brandingDisc' => true,
        ]));
    }

    /**
     * Отправляет стандартное оповещение менеджеру
     * @deprecated
     * @param string $subject Тема письма
     * @param string $balloon Текст в "облачке"
     * @param array $params Дополнительные параметры
     */
    public function sendManagerNotification(string $subject, string $balloon, array $params = [])
    {
        $viewParams = $params['viewParams'] ?? [];
        $emails = [];

        if (($params['toClient'] ?? true) && ($manager = $this->managerClient ?? null)) {
            if ($manager->email) {
                $emails[$manager->email] = $manager->name;
            }
        }
        if (($params['toDevelopment'] ?? true) && $manager = $this->managerDevelopment ?? null) {
            if ($manager->email) {
                $emails[$manager->email] = $manager->name;
            }
        }
        foreach ($emails as $email => $name) {
            Yii::$app->mailer->compose($params['template'] ?? [
                    'html' => 'partner-standard-manager-html',
                    'text' => 'partner-standard-manager-text',
                ], [
                    'balloon' => $balloon,
                    'partner' => $this,
                ] + $viewParams)
                ->setFromDefault()
                ->setTo([$email => $name])
                ->setSubject($subject)
                ->priority($params['priority'] ?? Message::PRIORITY_NORMAL)
                ->send();
        }
    }

    /**
     * Отправляет email менеджеру
     * @param string $subject Тема письма
     * @param array $widgets Виджеты
     * @param array $params Дополнительные параметры
     */
    public function sendManagerEmail(string $subject, array $widgets, array $params = [])
    {
        $emails = [];
        if (($params['toClient'] ?? true) && ($manager = $this->managerClient ?? null)) {
            if ($manager->email) {
                $emails[$manager->email] = $manager->name;
            }
        }
        if (($params['toDevelopment'] ?? true) && $manager = $this->managerDevelopment ?? null) {
            if ($manager->email) {
                $emails[$manager->email] = $manager->name;
            }
        }
        foreach ($emails as $email => $name) {
            Yii::$app->mailer->composeStandard([$email => $name], null, $subject, $widgets, $params)->send();
        }
    }

    /**
     * @param string $title Заголовок задачи
     * @param string|null $description Текст задачи
     * @param array $params Параметры задачи
     * @return string|null
     * @throws \yii\base\InvalidConfigException
     */
    public function sendManagerPlanfix(string $title, string $description = null, array $params = [])
    {
        $toDevelopment = ArrayHelper::remove($params, 'toDevelopment', false);
        $job = new PartnerJob([
            'partnerId'     => $this->id,
            'title'         => $title,
            'description'   => $description,
            'toDevelopment' => $toDevelopment,
            'params'        => $params,
        ]);
        if ($taskTime = ArrayHelper::remove($params, 'taskTime', null)) {
            $job->taskTime = $taskTime;
        }
        return Yii::$app->queue->push($job);
    }

    /**
     * Предпочтения по получаемым образцам
     * @return \yii\db\ActiveQuery
     */
    public function getSamplePreference(): \yii\db\ActiveQuery
    {
        return $this->hasOne(PartnerSamplePreference::class, ['partner_id' => 'id'])->inverseOf('partner');
    }

    /**
     * Бонусы на образцы
     * @return \yii\db\ActiveQuery
     */
    public function getSampleBonuses(): \yii\db\ActiveQuery
    {
        return $this->hasMany(SampleBonus::class, ['partner_id' => 'id'])->inverseOf('partner');
    }

    /**
     * Распределения образцов
     * @return \yii\db\ActiveQuery
     */
    public function getSampleDistributions(): \yii\db\ActiveQuery
    {
        /* @var $query ActiveQuery */
        $query = SampleDistribution::find(true);
        $query->primaryModel = $this;
        $query->link = ['partner_id' => 'id'];
        $query->multiple = true;
//        $query->inverseOf('partner');
        return $query;
    }

    /**
     * @return bool
     */
    public function getAllowCredit() : bool
    {
        return $this->credit_limit > 0 && $this->getHasCreditContract();
    }

    /**
     * Возвращает коэффициент скидки на нанесение
     * @return float
     */
    public function getBrandingDiscount(): float
    {
        $group = Discount::get(Discount::DISCOUNT_BRANDING);
        $discount = $this->discounts[Discount::DISCOUNT_BRANDING]->discount
            ?? $group->partnerCategoryDiscounts[$this->category_id]->discount
            ?? $group->discount
            ?? 0;
        return 1 - $discount / 10000;
    }

    /**
     * @fixme - убрать после лиги
     * @return bool|PromotionPartner
     */
    public function getIsInPromotionLeague()
    {
        return PromotionPartner::find()
            ->andWhere(['partner_id' => $this->id])
            ->andWhere(['promotion_id' => [473, 474,475,476]])
            ->one() ?? false;
    }

    /**
     * @param string $type
     * @return array|null
     */
    public function getBonusData(string $type)
    {
        foreach ($this->params['bonuses'] ?? [] as $bonus) {
            if ($bonus['type'] === $type) {
                return $bonus;
            }
        }
        return null;
    }

    /**
     * @param string $type
     * @return Coupon|null
     */
    public function getBonusCoupon(string $type)
    {
        if (array_key_exists($type, $this->_bonusCoupons)) {
            return $this->_bonusCoupons[$type];
        }

        $bonus = $this->getBonusData($type);
        if (empty($bonus['coupon'])) {
            return $this->_bonusCoupons[$type] = null;
        }

        /** @var Coupon $coupon */
        $coupon = Coupon::find()
            ->andWhere("ARRAY[:code]::varchar(32)[] && [[code]]")
            ->andWhere(['or', ['expire' => null], "[[expire]] > now()"])
            ->addParams([':code' => $bonus['coupon']])
            ->one();
        if (!$coupon) {
            return $this->_bonusCoupons[$type] = null;
        }
        $coupon->fullCode = $bonus['coupon'];
        return $this->_bonusCoupons[$type] = $coupon;
    }

    /**
     * Партнёр имеет собственное производство
     * @return bool
     */
    public function getHasOwnProduction(): bool
    {
        return boolval($this->params['hasOwnProduction'] ?? false);
    }

    /**
     * Возвращает необходимость исключить нанесение из цены товара
     * @return bool
     */
    public function getNeedToExcludeBranding(): bool
    {
        if ($this->getHasOwnProduction()) {
            return true;
        }
        //return !Yii::$app->settings->checkSeasonBrandingPartner($this);
        return false;
    }

    /**
     * Возвращает что партнёру не разрешено запускать нанесение в сезон
     * @return bool
     */
    public function getForbiddenSeasonBranding(): bool
    {
        return !Yii::$app->settings->checkSeasonBrandingPartner($this);
    }
}
